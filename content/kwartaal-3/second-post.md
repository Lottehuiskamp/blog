---
title: Dag 2
date: 2018-02-14
---

Ik wilde vooral de opdracht goed begrijpen dus ik besloot een debriefing te maken. Ook stond voor deze week op de planning dat we zouden beginnen met het plan van aanpak. Wat wilden we allemaal doen en hoe zouden we dat allemaal aanpakken? Na onze mooie teamnaam en teamlogo ontworpen te hebben hebben we gelijk besloten dat we er een stijl aan te hangen. Deze zouden we ook doorvoeren in het plan van aanpak. We moesten al gelijk bedenken hoe we de rest van de periode zouden inrichten en welke methoden we hiervoor zouden gebruiken. Ik vond het wel lastig om al gelijk zo ver vooruit te denken. We zijn goed begonnen met het plan van aanpak en de volgende studiodag gaan we hier verder mee.