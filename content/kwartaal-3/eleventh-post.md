---
title: Dag 12
date: 2018-05-16
---

Vandaag introduceer ik mijn creatieve sessie. Ik had een techniek bedacht een uitgelegd aan het team. In eerdere challenges heb ik een verslag gemaakt van de techniek, maar na het gebruik van de Insight Cards in periode 3 besloot ik de uitkomst van de sessie vast te leggen in een Insight Card. Nadat ik ook heb deelgenomen aan de creatieve techniek ‘backcasting’ die door een teamgenoot werd  voorgezeten konden we 4 ‘concepten’ meenemen naar de convergerende fase van het proces. Hier heb ik gekozen voor de techniek: ‘COCD-box’. Ik heb deze al eerder gebruikt dit jaar en ik vind het een goede techniek om te kijken wat het verschil is tussen de relevantie en orginaliteit van het concept. Na deze methode kwamen er 2 concepten uit. Hierna hebben we nog de voor- en nadelen van elk concept opgesteld en eindigde we met stemmen zodat er eerlijk een concept uit zou komen. Nu konden we beginnen met het uitwerken van het concept.