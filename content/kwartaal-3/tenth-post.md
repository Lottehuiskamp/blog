---
title: Dag 1 van de laatste periode!
date: 2018-05-09
---

Na de competentietetafels heb ik nog maar drie competenties te gaan! Deze periode zal ik mij bezighouden met de competenties
verbeelden en uitwerken, ideevorming en evalueren van testresultaten. Natuurlijk zal ik ook met de andere competenties bezig zijn want
we beginnen weer met onderzoeken!