---
title: Reflectie Design Challenge 4
date: 2018-06-03
---

Het jaar is snel voorbij gegaan. De laatste periode was anders dan de andere periodes. Ik kon rustiger te werk gaan omdat ik 
al 5 competenties behaald had. Dit gaf me veel rust. Het zorgde er ook voor dat mijn motivatie moeilijker was om op te brengen omdat
ik al veel dingen behaald had en ik daar dus niet heel hard meer voor moest werken. Maar voor mijn team heb ik het wel gedaan 
en gevraagd waar ik hen nog bij kon helpen. Ik vond het een leuk project en ben erg blij met het eindresultaat. Het team
heeft helaas een teamlid verloren in de laatste periode maar toch zijn we doorgegaan en hebben we het goed opgelost. Ik vond het leuk
met hen samen te werken en het verliep allemaal goed. Over het hele jaar vond ik het project van Paard en Fabrique het leukst en 
interessantst. Hierin was het hoogepunt het presenteren bij paard. Het was eer om dat mee te mogen maken.