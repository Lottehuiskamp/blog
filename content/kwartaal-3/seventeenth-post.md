---
title: Dag 17
date: 2018-06-01
---

Vandaag is de expo. We hebben nog een aantal posters uitgeprint en opgehangen. Helaas konden we de conceptposter niet meer printen omdat het op a1 moest en de rij was ontzettend lang. Deze hebben we maar gewoon op de computer laten zien. Mensen waren erg enthousiast over ons concept en wijzelf ook. Dit was dan de laatste dag en en nu nog even het leerdossier afmaken want die moet maandag ingeleverd worden!