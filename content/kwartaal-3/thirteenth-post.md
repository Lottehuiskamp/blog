--- 
title: Dag 13  
date: 2018-05-21 
---

Vandaag hebben we het concept duidelijk voor ons uitgewerkt. Ik ben zelf begonnen met het maken van een conceptposter die we ook kunnen gebruiken voor de expo. Jordan had al een begin gemaakt van de high fid app. In deze stijl ben ik verder gegaan en heb ik samen met Jordan een concept poster gemaakt. Ik heb ook feedback aan Robin gevraagd over de poster en hij vond het duidelijk en het concept kwam goed naar voren. Voor de rest van de weken staat het uitwerken van het high-fid prototype op de planning en producten maken voor de expo + testen natuurlijk.