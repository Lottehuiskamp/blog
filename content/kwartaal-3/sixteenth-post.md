---
title: Dag 16
date: 2018-05-30
---

Laatste studiodag van het jaar! Ik moest nog een paar puntjes op de i zetten voor vrijdag, dan is namelijk de expo. Ik heb de testrapportage afgemaakt en deze laten valideren bij Robin. Alle informatie stond erin en hij was in één keer goedgekeurd. Terwijl ik hiermee bezig was heeft Jordan nog de laatste wijzigingen in het prototype gedaan. Ik ben toen begonnen aan de uitgebreide conceptposter die voor de expo nodig was. Ik heb wat informatie verzameld en deze in de stijl van het prototype op de poster gezet. Nu zijn we klaar voor de expo