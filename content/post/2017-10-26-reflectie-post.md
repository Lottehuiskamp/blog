---
title: Reflectie op het kwartaal
date: 2017-10-26
---

Dat was het dan! De eerste Design Challenge is afgelopen. Ik kijk met een fijn en leuk gevoel terug op de afgelopen acht weken. Ik heb super veel geleerd op veel verschillende gebieden. De samenwerking in mijn team verliep goed. We hebben ons allemaal aan de taken gehouden en het was fijn om met mensen te werken die net zo gemotiveerd waren als ik. Dat hielp mij heel erg om serieus bezig te zijn. 
De kill your darling was voor mij de leerzaamste maar ook moeilijkste fase. Ik bleef veel hangen in het oude concept maar achteraf heeft het me super veel geleerd. Ik heb geleerd om verder te kijken dan het eerste concept en te beseffen dat het altijd beter kan. Dit kan op zoveel verschillende manieren, je moet er gewoon vanuit een andere hoek naar kijken.
De creatieve technieken toepassen was voor mij ook een eye-opener. Hierdoor besefte ik dat ik overal inspiratie uit kan halen als je je maar focust. Dit ga ik sowieso nog gebruiken in de andere kwartalen. 
Ik denk dat we een goed project hebben neergezet, goed geluisterd hebben naar de feedback die we hebben gekregen en we blij kunnen zijn met wat we hebben bereikt. Bij de peer feedback kreeg ik ook veel positieve feedback. Het was een fijn team om mee te werken. 
Dit was het dan voor de eerste Design Challenge. Ik zal blijven posten in de volgende kwartalen!