---
title: Dag 14
date: 2017-09-25
---

We begonnen ook deze maandag weer met het opschonen van het scrumbord. 
Elke maandag geven we deze een update. Hierna was dus ook duidelijk wat er allemaal nog gedaan moest worden voor deze week. 
We gingen weer verder waar we donderdag met de brainstorm gebleven waren. Helaas ging dit niet zo soepel als vorige week. 

<!--more-->

We zaten allemaal niet in de flow en kwamen niet op goede ideeën. We gingen verder met het gebruiken van creatieve technieken en mindmaps. 
Nadat we feedback hadden gevraagd aan Bob besloten we dit even achter ons te laten en iets anders te doen. 
We gingen kijken welke relevante spellen we deze week konden analyseren. 
