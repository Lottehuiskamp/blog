---
title: Dag 21
date: 2017-10-09
---


De laatste iteratie is vandaag begonnen. We kregen weer een nieuwe briefing en moesten aan de slag met als doel de expo over iets meer dan twee weken. Omdat we allemaal niet tevreden waren met hoe de presentatie was verlopen besloten we de nieuwe briefing nog even te laten liggen en eerst het concept helder en duidelijk te maken. Alles schreven we op en we hebben onszelf vragen gesteld die belangrijk waren en hierom dus beantwoord moesten worden. Hier hebben we de ochtend over gedaan en hierna was het voor iedereen duidelijk en konden we aan de briefing beginnen. Een nieuwe iteratie betekent ook een nieuwe scrum. Ons oude scrumbord was helaas verdwenen dus we hadden een nieuwe gemaakt. Wat er allemaal moest gebeuren hebben we opgeschreven en opgeplakt. Omdat we voor deze iteratie een prototype moeten maken en moeten exposeren op de expo is het van belang om een aantal dingen door te nemen voordat we beginnen. Wat willen we op de expo? Hier hadden we allemaal onze mening over gegeven en we hebben het even opzij gelegd omdat we eerst maar een naam en logo moesten maken/verzinnen. Na wat brainstormen over de naam kwamen we op Collect & Connect. Ik ging aan de slag met het schetsen van het logo en er kwam al snel iets uit waar we allemaal wel iets mee konden. Dit was een goede afsluiting van de dag.

