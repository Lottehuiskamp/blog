---
title: Dag 17
date: 2017-09-28
---


Na de design-tool les die ik in de ochtend had gevolgd, besloten we weer om samen te komen omdat vandaag het maken en bedenken van het prototype op de planning stond. 
Na de feedback die we hadden ontvangen over ons paper-prototype uit iteratie 1 besloten we om weer een paper-prototype te maken maar dan in de vorm van een ipad. 
We gingen opzoek naar materiaal en kozen schermen uit die voor de spelanalyse en presentatie goed waren om te laten zien. Iedereen heeft een aantal schermen ontworpen en deze hadden we al gelijk met kleur gemaakt. 
Dit ging redelijk snel en we liepen goed op schema. Het prototype was klaar en het enige wat we nog moesten doen was het maken van de spelanalyse en de presentatie. Dit waren we van plan om maandag uit te voeren.
