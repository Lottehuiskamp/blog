---
title: Dag 3
date: 2017-09-08 
---

Vandaag heb ik een moodboard gemaakt voor een thema dat we met het groepje hebben uitgekozen. 
Dit thema was city of sports. Ik heb foto’s van bepaalde sportevenementen die in Rotterdam plaatsvinden opgezocht en dit netjes bij elkaar gezet in Indesign. Ook heb ik op basis van de eerder gevonden foto’s en resultaten een klein kleuronderzoek erbij gevoegd.

