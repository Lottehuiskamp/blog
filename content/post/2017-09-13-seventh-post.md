---
title: Dag 7
date: 2017-09-13
tags: ["presentatie", "blog"]
---

We zijn deze dag begonnen met een stand-up met het team. We gingen bespreken wie wat zou gaan doen en wat de dagplanning zou zijn. 
In de studio werd gevraagd hoever iedereen was met de prototypes. 
Wij hadden deze de dinsdag afgemaakt. De teams die al klaar waren met het prototype konden het aan elkaar presenteren. 
Hier moesten we dus kort laten zien hoe ons spel in elkaar zat en hoe het werkt. Het was interessant en leuk om te zien wat de andere teams hadden en we merkten dat we goed op schema liepen. 
Na de presentatie wilden we beginnen met de spelanalyse. We besloten om verschillende rollen te geven in het team om de analyse zo goed mogelijk uit te voeren. 

<!--more-->

We waren met vier teamleden. Eén persoon maakte foto’s en was observant, de rest voerde het spel uit. 
Dit verliep goed en toen we terug op school waren konden we gelijk beginnen met de conclusie. 
Er zijn meerdere dingen duidelijk geworden uit de spelanalyse en deze hebben we genoteerd en verwerkt in de spelanalyse. 
Aan het eind van de dag kregen we les van onze studiecoach. Hier moesten we het formulier voor het dit ben ik profiel invullen. 
Hier hebben we de dag mee afgesloten.