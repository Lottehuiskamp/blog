---
title: Dag 15
date: 2017-09-26
---


Na het hoorcollege van vandaag besloten we wat langer op school te blijven om de spelanalyses van gister af te maken. 
We wilden een actief spel doen omdat we dit ook in ons eigen spel wilden verwerken. We hebben de rollen verdeeld en demi en ik zouden het spel Geocache gaan spelen. 
Ik had de app gedownload en hier begon ons spel. Demi zou observeren en we zouden naderhand samen de spelanalyse maken en bespreken. Het spel was om op zoek te gaan naar verborgen elementen in de stad. 
Dit paste goed bij ons spel omdat wij de stad Rotterdam ook wilden gebruiken als de basis. Nadat we twee elementen hadden gevonden besloten we dat we genoeg informatie hadden om de spelanalyse te maken. 
Deze zijn thuis uitgewerkt.

