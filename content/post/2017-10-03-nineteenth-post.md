---
title: Dag 19
date: 2017-10-03
tags: ["presentatie"]
---

Demi en ik hadden vandaag afgesproken om de spelanalyse samen uit te werken omdat wij als enige hier bij waren geweest en het spel heel onduidelijk was. 
We hebben alles zo goed mogelijk verwerkt en de spelregels hieraan aangepast omdat deze nog gebaseerd waren op het vorige spel. 
Ook wilden we vandaag de presentatie afhebben en hier hebben we met het team contact over gehad en aangegeven wat er misschien nog anders moest. 
We hadden besloten de presentatie niet met het hele groepje te doen omdat de tijd die we hebben heel kort was en het wel duidelijk moest zijn. Joshua, Rosa en Beau zouden presenteren en wij zouden een andere keer aan de beurt zijn.

