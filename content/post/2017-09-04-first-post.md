---
title: First post!
date: 2017-09-04
tags: ["welcome", "blog", "blogging"]
---

Vandaag was de eerste studiodag met het team. We hebben teamafspraken en doelen besproken en deze heb ik uitgewerkt op de computer. Ook moest er een planning gemaakt worden. Deze hebben wij via de ‘scrum’ methode gemaakt. 
We hebben ook de doelgroep voor de game bepaald. Onze doelgroep is de eerstejaarsstudenten van de studie Fysiotherapie. 
Verder hebben we bedacht welke onderzoeksvragen we zouden kunnen gebruiken, hier heb ik een lijst gemaakt van meerdere vragen. We hebben de beste vragen van iedereen in één lijst gezet.

<!--more-->

---
Eerste dag, wat gaan we doen?
---

Mijn onderzoeksvraag was: ‘Zijn er spellen waarbij je de stad (Rotterdam) kan ontdekken?’ 
Om meer te weten te komen over de doelgroep heeft iemand uit het team een aantal vragen opgesteld. Deze heb ik uitgewerkt naar een online enquete die we kunnen verspreiden onder de doelgroep. 
Als laatst moesten we met het team nog een poster maken waar al onze kwaliteiten, regels en afspraken op kwamen te staan. 


