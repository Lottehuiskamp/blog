---
title: Dag 16
date: 2017-09-27
---


Vandaag wilden we ons richten op het concept. Deze moest vandaag wel af zijn, zeker omdat we maandag al vastliepen op het opnieuw bedenken van het concept en de presentatie al over een week was. 
We wilden de hele ochtend hieraan besteden zodat we in de middag verder konden met de overige spelanalyses van de relevante spellen en het maken van de spelregels. De spelregels heb ik samen met demi opgesteld, hier was het belangrijk om de spelregels niet te snel te verwarren met het spelverloop en de speluitleg. 
Ook hadden we vandaag feedback gekregen op ons ingeleverde bestand van ons team van iteratie 1. De feedback was duidelijk en we waren er als team tevreden over en we konden het zeker toepassen in de nieuwe iteratie. 

