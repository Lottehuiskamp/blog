---
title: Dag 5
date: 2017-09-11
---

Deze week ben ik de teamcaptain. We begonnen vandaag met een stand-up om te bespreken waar elke groepjes ongeveer waren, of mensen tegen problemen aan lopen en of we goed op schema lopen. 
Na deze meeting hebben we met het groepje een stand-up gedaan en het scrumbord opnieuw gemaakt om alles van deze week erin te verwerken. 
We hebben besproken dat we vandaag een concept wilden hebben voor de game en dat we zouden beginnen aan het paper-prototype.

<!--more-->

Helaas liepen we een beetje vast en hebben we aan Bob hulp gevraagd om ons een beetje op weg te helpen. Hij liet ons vanuit een andere hoek kijken naar ons concept. 
Dit is voor mij wel heel leerzaam geweest! Door zo gericht te denken kan je uit iets heel kleins heel veel inspiratie halen. Dit zal ik dan ook zeker meenemen naar andere brainstorms voor de rest van deze studie.