---
title: Dag 18
date: 2017-10-02 
---

Vandaag moesten we nog de laatste loodjes leggen om alles af te maken voor deze iteratie en natuurlijk voor de presentatie van woensdag.
In de ochtend kregen we een kleine introductie en hebben we alle documenten die al af waren in één document gezet zodat we deze gemakkelijk konden inleveren woensdag. 
In de middag moest het groepje opsplitsen omdat een aantal mensen een workshop gingen volgen. Demi en ik zouden ons richten op de spelanalyse en iemand anders zou al een start maken met de presentatie. 

<!--more-->

We hadden een groepje gevraagd om ons spel te spelen en we kwamen er al snel achter dat het concept erg onduidelijk was en het was niet te leuk om te spelen. 
De feedback van het groepje hebben we allemaal verwerkt in de spelanalyse en we besloten weer met het groepje rond te tafel te gaan zitten om de feedback te vertellen en het concept een stuk te verfijnen dat het aantrekkelijker was om te spelen. 
We hadden een aantal elementen toegevoegd en waren hier zelf tevreden over. Hierop hadden we de presentatie gebaseerd en waren we klaar voor woensdag.
