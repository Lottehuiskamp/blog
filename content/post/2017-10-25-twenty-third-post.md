---
title: De laatste dag!  
date: 2017-10-25
---

De dag van de expo. Het opbouwen begon om 9:30. Iedereen had de spullen meegenomen wat we hadden afgesproken. De apps werkten, de posters hingen en de tafel was opgemaakt.
Klaar voor de expo. Mensen waren enthousiast en het was leuk om te doen. Ik vond het vooral leuk om eindelijk goed te kunnen zien wat alle klasgenoten en teams hadden gemaakt.
Er zaten leuke spellen tussen. Helaas hebben we niet gewonnen maar het was een leuke afsluiting!