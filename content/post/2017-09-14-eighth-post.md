---
title: Dag 8
date: 2017-09-14
---

We zijn allemaal individueel begonnen aan een design-tool les. Ik had gekozen voor Sketch omdat ik nog geen ervaring heb met dit programma. 
Ook denk ik dat dit een goed programma kan zijn om te gebruiken in de designchallenge. Dit kan een pluspunt zijn voor het team en de challenge. 
Na deze les kwam het groepje samen en zochten we een plekje om verder te werken aan de spelanalyse. 

<!--more-->

Aan het eind van de ochtend hadden we ook al een opzet voor de presentatie. Nu hadden we alle losse elementen af en was het van belang om alles in één document te zetten zodat we dit aan het eind van de dag konden inleveren. 
Alle teamafspraken en foto’s van het scrumbord werden in één document gezet. 
Halverwege de middag hadden we alles af en besloten we om het document in te leveren op N@tschool.