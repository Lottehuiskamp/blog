---
title: Dag 20
date: 2017-10-04

---


De dag van de presentatie. We moesten als derde presenteren voor een andere klas waar wij niet mee in de studio zaten. De presentatie zag er goed uit maar we kwamen erachter nadat we mondelinge feedback hadden gehad dat het concept toch nog niet heel duidelijk voor ons was omdat we het laatste moment nog hadden geswitcht. De feedback was positief maar we moesten voor onszelf alles op een rijtje zetten en de puntjes op de i zetten. Hierna hebben we nog naar andere teams gekeken en er zaten leuke spellen tussen. De iteratie was afgelopen en we hebben het document weer ingeleverd op natschool. Op maandag begint de laatste iteratie en gaan we het concept tot op de puntjes uitwerken


