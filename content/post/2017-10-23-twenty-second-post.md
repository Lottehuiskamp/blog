---
title: Dag 22
date: 2017-10-23
---


De laatste week is aangebroken! Woensdag is de expo en daarmee ook de laatste dag met het team en van Design Challenge 1.
Er moesten nog een aantal dingen gedaan worden. In de vakantie heb ik samen met demi de app gemaakt en nu was het de bedoeling om deze af te maken voor de spelanalyse die voor vandaag op de planning stond.
Er was helaas een tegenvaller want de interactieve pdf die ik had gemaakt werkte niet op de Ipad. Ik heb met Bob een groot deel van de ochtend gezeten om te kijken of we hem op een ander manier aan de praat konden krijgen.
Gelukkig is dit aan het eind van de ochtend gelukt en konden we de spelanalyse spelen.

Alles was klaar voor de expo, we zijn er klaar voor!