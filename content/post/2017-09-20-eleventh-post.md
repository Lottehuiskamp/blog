---
title: Dag 11
date: 2017-09-20
---

We begonnen vandaag weer met de stand-up, ook nu gingen we bespreken wat er deze dag op de planning staat. 
Het was de bedoeling dat we de spelanalyse zouden maken over drie bestaande spellen. Een deel van de groep had vandaag een workshop gepland. 
We hadden gekozen voor drie verschillende spellen. Een bordspel, ‘tv-kantine’, een kaartspel, ‘set’, en een basis spel, ‘pesten’. 
Bij de tv-kantine en set hebben we dit met het groepje geanalyseerd en uitgewerkt en bij het kaartspel set heb ik dit samen met een teamgenoot gedaan omdat de rest bezig was met de workshop. 
