---
title: Dag 2
date: 2017-11-20
---

Samen met Sico ben ik begonnen aan de debriefing. We vonden het belangrijk dat het team dit zou begrijpen voordat we begonnen met het concept. We hadden de rollen verdeeld. 2 deden de debriefing en 2 de merkanalyse. Nadat Sico en ik deze hadden geschreven vroegen we feedback aan Bob en Isabella. Bob vond de debriefing erg duidelijk en gaf aan dat we misschien kopjes konden gebruiken om het duidelijker te maken. Nadat ik deze feedback had verwerkt ging in naar Isabella en zij gaf aan dat we het meer naar Fabrique en Paard moesten schrijven. Ik had de schrijfstijl aangepast en me gelijk ingeschreven voor de validatie. Hierna besloot ik om een onderzoek te starten naar Paard en hun concurrenties online. Hier heb ik een visueel onderzoek van gemaakt. Woensdag zou ik hierover feedback vragen.
