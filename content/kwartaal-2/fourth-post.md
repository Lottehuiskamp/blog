---
title: Dag 4
date: 2017-11-27
---

We begonnen weer met een standup. Er stond veel op het progamma vandaag. Ik had een workshop van Bob gepland over HTML/CSS en tegelijkertijd had ik een validatie van de debriefing. Nadat ik de feedback van vorige week had verwerkt vond ik de debriefing goed genoeg om te laten valideren. Dit klopte ook en het was voldoende voor het leerdossier. Hierna had ik een deel gemist van de workshop maar ik kon gelukkig goed aanhaken. Ik had al eerder met HTML gewerkt maar dat was al een tijdje geleden. Het was goed om er weer eens mee te werken en dingen door een andere docent uitgelegd te krijgen. Met het team bekeken we samen wat er in de workshop was besproken en gingen we verder te werk. 
