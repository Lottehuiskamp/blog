---
title: Dag 3
date: 2017-11-22
---

Nadat ik maandag was begonnen aan mijn onderzoek over de concurrentie, besloot ik vandaag er even met Bob naar te kijken. Hij vond dat het een goed onderzoek was en dat ik misschien in een schema nog beter kon vertellen wat de conclusie was. Dit is wat ik toen heb gedaan. Ook ging ik aan de slag met een styleguide voor het team. We willen al onze documenten en onderzoek in dezelfde stijl presenteren. Ik besloot daarom een styleguide te maken. Hierin stonden dingen als de grootte van de koppen en platte tekst. Ook de kleurcombi’s die we konden gebruiken waren aangegeven. Het team vond dit een goed idee, nu konden we aan het eind alles in één document zetten.
