---
title: Reflectie
date: 2018-01-18
---

Wat een kwartaal! Wat is het weer snel gegaan. Ik vond het ten eerste een super leuke opdracht. Ik ben heel blij wat voor resultaat mijn team en ik hebben neergezet. Na de eerste pitch kregen wij veel negatieve feedback en toch hebben we nu echt een tof product neergezet waar we goede en positeve feedback op hebben gekregen. Alles verliep goed. De doelen die ik voor mijzelf had opgesteld heb ik allemaal gehaald. In het team ging de communicatie en samenwerking heel goed. Er zijn veel producten in één keer gevalideerd. Dit is denk ik ook een gevolg van de goede samenwerking. Nu gaan we weer op naar het volgende project!