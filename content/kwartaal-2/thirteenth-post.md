---
title: Dag 13
date: 2018-01-08
---

Happy new year! 
De eerste schooldag weer na de vakantie. Na alle vakantieverhalen te hebben gedeeld met de klas zijn we aan de slag gegaan. We lopen gelukkig heel goed op schema en vandaag stond vooral in het teken van een test afleggen en het testplan afmaken. We hadden hier dus de hele dag de tijd voor!