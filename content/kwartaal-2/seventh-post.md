---
title: Dag 7
date: 2017-12-06
---

Vandaag stond de pitch op het programma. Om 11:15 waren wij aan de beurt.
Sico zou de pitch geven. We hebben van ons bandje een prototype gemaakt. 
Met een magneetje hebben we het bandje vastgemaakt. 
We besloten de pitch en de feedback te filmen zodat we dit altijd nog terug
konden kijken. 
Ze vonden het concept helaas niet goed. Ze twijfelden of het de bezoeker zou laten terugkeren.
Ze zeiden dat het een soort klantenkaart was. Dit was helaas niet wat wij voor ogen hadden. 
Verder hebben we die dag een planning gemaakt voor de aankomende weken.
