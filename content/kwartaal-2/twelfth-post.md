---
title: Dag 12
date: 2017-12-20
---

Vandaag was de laatste dag voor de kerstvakantie. We zouden vandaag ons prototype testen op basis van het testplan wat we gister hebben geschreven. We hadden twee tweedejaars gevraagd die bij ons in de doelgroep pasten. Zij hebben het product getest en ik heb het gefilmd. Alle vragen zijn die we hadden opgesteld in het testplan hebben we ook gesteld aan de testpersonen. Deze heeft Sico genoteerd en ik heb het gefilmd. Hiervoor had ik nog een workshop gevolgd van Jiska en Dennis over HTML/CSS. Hier heb ik in de middag nog aan gewerkt en hierna heb ik mijn blog bij gewerkt.