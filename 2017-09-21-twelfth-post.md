---
title: Dag 12
date: 2017-09-21
---

In de ochtend was ik begonnen met het design-tool vak, sketch. Nadat dit was afgelopen besloten we met het team nog even samen te brainstormen over het concept. Hiervoor wilden we graag een keer iets anders doen en begonnen we met de creatieve technieken. 
We zaten op de eerste verdieping en hier hingen allerlei verschillende lampen waar we onze inspiratie vandaan haalden om verschillende ideeën te bedenken. Al vrij snel kwamen we uit op een leuk element wat we graag wilden laten terugkomen in het concept. 
Dit gaf bij ons aan dat het super veel scheelt om op andere manieren na te denken. 

<!--more-->

Bij iteratie 1 hadden we namelijk moeite met het bedenken van het concept en nu kwamen we al vrij snel op een idee. 
Nadat we hiermee klaar waren we klaar met school aangezien ons werkcollege in de middag verviel. 
Thuis heb ik een start gemaakt voor het ditbenik-profiel die we vrijdag voor 24:00 moesten inleveren.