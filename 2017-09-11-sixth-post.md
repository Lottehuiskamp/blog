---
title: De zesde dag alweer!
date: 2017-10-12
tags: ["prototyping", "blog"]
---

Vandaag hebben we nadat we een hoorcollege hadden gevolgd over ‘verbeelden’, afgesproken met het team om verder te werken aan de design-challenge. 
Op het programma stond dat we het prototype zouden afronden zodat we deze morgen aan de klas konden presenteren. 
We hadden allemaal eerder nog nooit een prototype gemaakt, dus het eerste wat we deden was het bedenken hoe we het gingen aanpakken. 
Gingen we het met papier doen of met bijvoorbeeld karton. Gezamenlijk hadden we besloten dat we een iPhone zouden maken van karton. 
Hierin zouden we dan de verschillende schermen van de app kunnen laten zien. 
Dit hebben we op losse papiertjes gemaakt die even groot zijn als het scherm. 

We begonnen met het tekenen van de grootte van de iPhone op karton, deze hebben we twee keer uitgesneden en op elkaar geplakt. 
De bovenkant hebben we niet vastgemaakt zodat we de papiertjes er steeds in en uit kunnen schuiven. 
Nadat we alle schermen af hadden gemaakt, hebben we deze op volgorde in het frame geplaatst. 
