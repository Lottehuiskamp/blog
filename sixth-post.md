---
title: Dag 6
date: 2017-12-4
---

Vandaag begonnen we met een standup om te bespreken wat de planning was vandaag. Ik zou beginnen met het schrijven van een testplan. Ook zouden we de ontwerp criteria laten valideren. Het was alleen zo druk bij de validatie en er was veel onduidelijkheid. We kregen wat algemene feedback en de validatie was verplaatst naar woensdag 6 december. 
